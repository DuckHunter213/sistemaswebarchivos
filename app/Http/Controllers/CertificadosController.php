<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estaticas\Certificado;

class CertificadosController extends Controller
{ 
  public function __construct()
  {
    $this->middleware(['auth', 'verified']);
  }

  public function index()
  {
    return view('certificados/index');
  }

  public function create(Request $request){
    //Recibir la contraseña del usuario para sus certificados
    $password = $request->input('password');
    
    $certificado = new Certificado();
    $user = auth()->user()->email;
    //Si no existen las llaves del usuario
    if(!$certificado->existenLlaves($user)){
      
      $llaves = $certificado->generarLlaves();
      $llavePrivada = $certificado->generarLlavePrivada($llaves);
      $llavePublica = $certificado->generarLlavePublica($llaves);
      //Guarda las llaves publicas y privadas para cuestiones de desarrollo
      $certificado->guardarLlaves($llavePublica,$llavePrivada,$user);
      
      $llaveCifrada = $certificado->cifrarLlavePrivada($llavePrivada,$password);
      
      if(!$llaveCifrada == false){///
        $certificado->guardarLlave($llaveCifrada,"privateEncryptKey.pem",$user);
        $certificado->firmarCertificado(auth()->user()->name, auth()->user()->email, $llavePrivada);
        auth()->user()->certificated = true;
        auth()->user()->save();

        return redirect()->route('certificados.index');
      }else{
        //TODO: Manejar la excepcion y mostrarla al usuario.
        clock()->debug("Ya existen las llaves");
        return redirect()->route('certificados.index');
      }
      return redirect(route ('certificados.index'));
    }
  }
}
