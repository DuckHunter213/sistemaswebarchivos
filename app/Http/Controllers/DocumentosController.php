<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Estaticas\Certificado;
use Spipu\Html2Pdf\Html2Pdf;
use App\Documento;
use App\firmantesSolicitados;
use App\User;
use Mail;

class DocumentosController extends Controller
{ 
  public function __construct()
  {
    $this->middleware(['auth', 'verified']);
  }

  public function index()
  {
    $documentos = Documento::where('solicitante',auth()->user()->email)->get();
    $documentosAFirmar = firmantesSolicitados::where('correoFirmante',auth()->user()->email)->get();
    $documentosCompartidos = [];
    foreach ($documentosAFirmar as $compartido) {
      $documento = Documento::find($compartido->idDocumento);
      array_push($documentosCompartidos,$documento);
    }
    return view('documentos/index', compact('documentos','documentosCompartidos'));
  }

  public function details(Request $request){
    $user = auth()->user()->email;
    $idDocumento = $request->input('documento');
    $documento = Documento::find($idDocumento);
    $firmantes = firmantesSolicitados::where('idDocumento',$documento->id);
    $firmas = [];
    //$firmantes = firmantesSolicitados::find(1);
    //dd($firmantes->correoFirmante);
    $solicitante = User::where('email', $documento->solicitante)->first();
    
    //clock()->debug($solicitante);

    return view('documentos/details', compact('documento','solicitante','firmantes','firmas'));
  }


  public function subirDocumento(Request $request){
    request()->validate(['file' => 'documento']);
    $request->file('documento');
    $nombreDocumento = $request->file('documento')->getClientOriginalName();
    $nombreDocumento = str_replace(" ", "_", $nombreDocumento);
    $email = Auth::user()->email;
    $documento= new Documento([
      'nombreDocumento'=> $nombreDocumento,
      'solicitante'=> $email,
    ]);
    $documento->save();
    $request->file('documento')->storeAs('public/'.$email."/documentos/".$documento->id, $nombreDocumento);
    $ruta = "app/public/".$email."/documentos/".$documento->id.'/'.$nombreDocumento;
    $file = storage_path($ruta);
    $hash = hash_file ('md5',$file);
    $documento -> huellaDigital=$hash;
    $ruta = "../storage/".$email."/documentos/".$documento->id.'/'.$nombreDocumento;
    $documento -> urlDocumento = $ruta;
    $documento -> save();
    $numeroFilas = $request->get('numeroFilas');
    for ($x = 0; $x < $numeroFilas; $x++) {
      $correoFirmante=$request->get('firmante'.$x);
      $firmante= new firmantesSolicitados([
        'correoFirmante' => $correoFirmante,
        'idDocumento' => $documento->id,
      ]);
      $data = array('name'=>$correoFirmante , 'archivo'=>$nombreDocumento);
      Mail::send('emails.contact', $data, function($message) use ($correoFirmante){
        $message->to($correoFirmante, 'Firmante solicitado')->subject
           ('Solicitud de firma');
        $message->from('signatilitteris@gmail.com','Administración SignatiLitteris');
     });
      $firmante -> save();
    }
    return redirect('documentos/index');
  }


  public function create(){
    return view('documentos/create');
  }

  public function firmarDocumento(Request $request){
    request()->validate(['file' => 'firmaElectronica']);
    $llaveCifrada = file_get_contents($request->file('firmaElectronica'));
    $password = $request->input('passwordCertificated');
    $documento = Documento::find($request->input('idDocumento'));
    $huellaDocumento = $documento->huellaDigital;
    $direccion = "public/".$documento->solicitante."/documentos/".$documento->id.'/';
    $certificado = new Certificado();
    $firma = $certificado->descifrarLlavePrivada($llaveCifrada,$password);
    $signature = $certificado->firmarDocumento($huellaDocumento,$firma);
    if($signature){
      //Aqui ya se firmo el documento se debe guardar la llave en un .dat
      $usuario = auth()->user()->email;
      $nombrefirma = $usuario.".dat";
      //Firma, Ubicacion de guardado, nombre de la firma
      $certificado->guardarFirma($signature,$nombrefirma,$direccion);
    }else{
      clock()->debug("No se genero la llave del documento");
    }    
    return redirect('documentos/index');
  }

  public function generarPDF(Request $request){

    $idDocumento = $request->input('idDocumento');
    $documento = Documento::find($idDocumento);
    $solicitante = User::where('email', $documento->solicitante)->first();
    //Aquí se empieza a generar el pdf
    $html2pdf = new Html2Pdf('P', 'Letter', 'es', true);
    //Evitar las modificaciones en el text area
    $html2pdf->pdf->SetProtection(array('modify','annot-forms'));
    $html = "<h1>Hoja de firmas (Signati Litteris)</h1>";
    $html = $html."<h2> Huella del documento: $documento->huellaDigital </h2>";
    //Anexando firmas disponibles
    $ruta = "public\\".$documento->solicitante."\\documentos\\".$idDocumento."\\";
    $files = Storage::files($ruta);
    if(!Storage::disk('local')->exists($ruta."documentoSigned.pdf")){
      foreach ($files as $firma) {
        //Datos del archivo        
        $nombre = basename($firma);
        $extension = substr($nombre, -4);
        $correo = substr($nombre, 0, -4);
        //solo si son archivos de firma
        if($extension == ".dat"){
          $contents = Storage::get($firma);
          $html = $html."<h3>Firma de ".$correo."</h3>";
          $html = $html."<div style='width:100%; display:block;'><textarea rows='5' cols='85' disabled readonly>".base64_encode($contents)."</textarea></div>";
        }
      }
      $html2pdf->writeHTML($html);
      $pdf2 = "firmas.pdf";
      Storage::disk('local')->put($ruta.$pdf2,$html2pdf->output('documento.pdf', 'S'));
      $archivoSalida = "documentoSigned.pdf";
      $pdf1 = $documento->nombreDocumento;
      //Generando el documento
      $comandoRuta = "combinarPDF ".base_path("storage/app/public/").$documento->solicitante."/documentos/".$documento->id."/ ".$archivoSalida." ".$pdf1." ".$pdf2;
      clock()->info($comandoRuta);
      $texto = System($comandoRuta,$salida);
      //TODO: se tiene que depurar los errores, de salida
      clock()->debug($salida);
      //ultima linea
      clock()->debug($texto);
    }
    return response()->download(storage_path("app/public/".$documento->solicitante."/documentos/".$documento->id."/documentoSigned.pdf"));
  }

  public function PDFFirmado(Request $request){
    clock()->debug("app/public/".$request->input('usuarioEmail')."/documentos/".$request->input('idDocumento')."/documentoSigned.pdf");
    return response()->download(storage_path("app/public/".$request->input('usuarioEmail')."/documentos/".$request->input('idDocumento')."/documentoSigned.pdf"));
    //return response()->download(storage_path("app/public/".$documento->solicitante."/documentos/".$documento->id."/documentoSigned.pdf"));
  }

  public function test(){
    $ruta = "ejemplo para quitar todos los espacios.pdf";
    $files = str_replace(" ", "_", $ruta);
    dd($files);
  }
}