<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UserController extends Controller
{

  public function __construct()
  {
    $this->middleware(['auth', 'verified'])->except('index');
  }
  
  public function index()
  {
    $permisos = auth()->user()->getAllPermissions();
    return view('user/index', compact('permisos'));
  }

  public function giveAdminControl()
  {
    if(false){
      $role = Role::create(['name' => 'coordinador']);
      $role = Role::create(['name' => 'firmante']);
      $role = Role::create(['name' => 'administrador']);
      //Creando permisos
      $permission = Permission::create(['name' => 'subir documento']);
      $permission = Permission::create(['name' => 'firmar documento']);
    }

    $users = auth()->user()->assignRole('coordinador');
    $users = auth()->user()->assignRole('administrador');
    $users = auth()->user()->assignRole('firmante');
    return redirect('user/index');
  }


  public function details($email)
  {
    $user = User::where('email',$email);
    return view('/users/details',compact('user'));
  }

  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
  }
}
