<?php

namespace App\Http\Controllers;

use App\Events\HolaPusherEvent;
use Illuminate\Http\Request;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class ChatController extends Controller
{
  public function __construct()
  {
    $this->middleware(['auth', 'verified']);
  }

  public function index()
  {
    /*
    this.pusher = new Pusher('b7143dca9285dd0e25e9');
    this.pusherChannel = this.pusher.subscribe('user.' + USER_ID);
    this.pusherChannel.bind('App\\Events\\ServerCreated', function(message) {
      console.log(message.user);
    });
    */
    return view('chat/index');
  }

  public function mandarMensaje(Request $request){
    $message = $request->input('message');
    broadcast(new HolaPusherEvent($message))->toOthers();
    return ['status' => 'Message Sent!'];
  }

  public function recibirMensaje(){
    
  }
  
}
