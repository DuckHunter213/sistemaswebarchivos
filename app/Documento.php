<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
  protected $fillable = [
    'nombreDocumento','solicitante', 'urlDocumento', 'huellaDigital',
  ];
}
