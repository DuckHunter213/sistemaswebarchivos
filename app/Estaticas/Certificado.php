<?
namespace App\Estaticas;
use Illuminate\Support\Facades\Storage;

class Certificado
{
  private $configargs;

  function __construct(){
    //configuraciones necesarias para trabajar los certificados con servidor windows
    $this->configargs = array(
      "config" => "C:/xampp/php/extras/openssl/openssl.cnf",
      'private_key_bits'=> 2048,
      'default_md' => "sha256",
      "x509_extensions" => "v3_req",
    ); 
  }
  
  //Genera certificados con la extension .crt
  public function firmarCertificado($nombreUsuario, $correoUsuario, $privkey){
    
    $dn = array(
      "countryName" => "MX",
      "stateOrProvinceName" => "Veracruz",
      "localityName" => "Xalapa",
      "organizationName" => "Signati Litteris",
      "organizationalUnitName" => "Signati Litteris Team",
      "commonName" => $nombreUsuario,
      "emailAddress" => $correoUsuario
    ); 
    //clock()->info($dn);
    $csr = openssl_csr_new($dn, $privkey,$this->configargs);
    openssl_csr_export($csr, $csrout);
    //clock()->info("Certificado: ".$csrout);
    try {
      $autoridadCertificadora = file_get_contents(base_path("resources\certificated\server.crt"));
      $privkey2 = file_get_contents(base_path("resources\certificated\server.key"));
      //mostrar las llaves
      //clock()->info($autoridadCertificadora);
      //clock()->info($privkey2);
      $x509 = openssl_csr_sign($csrout, $autoridadCertificadora, $privkey2, 365, $this->configargs);
      if ($x509) {
        openssl_x509_export($x509, $certout);
        //clock()->debug($certout);
        $this->guardarLlave($certout,"certificado.cer",$correoUsuario);
      }else{
        while (($e = openssl_error_string()) !== false) {
          //clock()->debug($e);
        }
        //clock()->debug("No se genero el certificado");  
      }
    } catch (Exception $e) {
      //clock()->debug($e);  
    }
  }

  //genera el par de llaves
  public function generarLlaves(){
    //Crear el par de llaves
    $res=openssl_pkey_new($this->configargs);
    return $res;
  }
  
  //Método utilizado para crear la llave privada
  public function generarLlavePrivada($res){
    //Get private key
    openssl_pkey_export($res, $privKey,NULL,$this->configargs);
    return $privKey;
  }

  //desifrar la llave privada
  public function descifrarLlavePrivada($llaveCifrada,$password){
    $llavePrivada = openssl_get_privatekey($llaveCifrada,$password);
    return $llavePrivada;
  }
  
  public function cifrarLlavePrivada($llavePrivada,$password){
    
    /*openssl_pkey_export(
      llavePrivada,
      Lugar donde exporta la llave,
      contraseña del certificado,
      configuraciones necesarias
    )*/
    if(!openssl_pkey_export($llavePrivada,$llaveCifrada,$password,$this->configargs)) {
      return false;
    }else{
      return $llaveCifrada;
    }
  }

  public function generarLlavePublica($res){
    // Obtienes la llave publica de esta función
    $publicKey=openssl_pkey_get_details($res);
    $publicKey=$publicKey["key"];
    return $publicKey;
  }

  //Método despreciado utilizar mejor el de guardar llave unica pasando como parametro el nombre
  public function guardarLlaves($llavePublica,$llavePrivada,$user){
    $directorio = 'public/'.$user.'/certificados/';
    Storage::disk('local')->put($directorio.'privateKey.key', $llavePrivada);
    Storage::disk('local')->put($directorio.'publicKey.pem', $llavePublica);
  }
  //la cadena de la llave, el nombre y el usuario.
  public function guardarLlave($llave,$nombre,$user){
    $directorio = 'public/'.$user.'/certificados/';
    Storage::disk('local')->put($directorio.$nombre,$llave); 
  }

  public function guardarFirma($llave,$nombre,$directorio){
    Storage::disk('local')->put($directorio.$nombre,$llave); 
  }

  public function existenLlaves($user){
    $exists = true;
    $directorio = 'public/'.$user.'/certificados/';
    $exists = Storage::disk('local')->exists($directorio.'privateKey.key'); 
    $exists = Storage::disk('local')->exists($directorio.'publicKey.pem'); 
    return $exists;
  }

  //TODO: Esto debe de ser guardado en un archivo signature_[usuario][documento].dat
  public function firmarDocumento($huellaDocumento,$firma){
    if(openssl_sign($huellaDocumento,$firmaDocumento,$firma,OPENSSL_ALGO_SHA256)){
      return $firmaDocumento;
    }else{
      return false;
    }
  }

}

?>