<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class correoFirma extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $contact;

    public function __construct()
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
          ->from('signatislitteris@gmail.com') // 送信元
          ->subject('Firmate') // メールタイトル
          ->view('contact.mail') // どのテンプレートを呼び出すか
          ->with(['contact' => $this->contact]);
    }
}
