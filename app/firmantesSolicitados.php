<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class firmantesSolicitados extends Model
{
    protected $fillable = [
        'correoFirmante','idDocumento','estado',
      ];

}
