<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $permissions = [
       'cordinador',
       'firmante'
    ];
    foreach ($permissions as $permission) {
      Permission::create([
        'name' => $permission,
        'guard_name' => $permission,
      ]);
    }
  }
}
