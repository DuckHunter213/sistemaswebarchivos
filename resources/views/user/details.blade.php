@extends('layouts.app')
@section('content')
<div class="container">
  @if($permisos)
    <h1>El usuario no tiene permisos de nada </h1>
  @else
    @foreach ($permisos as $permiso)
      <p>{{$permiso}}</p>
    @endforeach
  @endif  
</div>
@endsection