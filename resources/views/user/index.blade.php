@extends('layouts.app')
@section('content')
<div class="container">
  <div class="form-row align-items-center">
    <label class="col-12" for="usuario">Buscar un usuario distinto, Usuario actual: <strong> {{auth()->user()->email}}</strong></label>
    <input class="form-control col" type="text" name="usuario" placeholder="buscar usuario a administrar">
    <button class="btn btn-primary col-1"><i class="fas fa-search"></i></button>
  </div>
    <h1> Roles de usuario </h1>
    <button class="btn btn-danger">Boton ultra secreto para dar superpoderes al usuario actual</button>
    <div class="row">
      <div class="col-sm-12 col-lg-6">
        <h2>Roles</h2>
        <ul>
          <li>Administrador</li>
          <li>Coordinador</li>
          <li>Firmante</li>
        </ul>
      </div>
      <div class="col-sm-12 col-lg-6">
        <h2>Permisos</h2>
        <ul>
          <li>Firmar Documentos</li>
          <li>Ver Estatus de Documentos</li>
          <li>Revocar Certificados</li>
          <li>Renovar Certificados</li>
        </ul>
      </div>
    </div>
</div>
@endsection