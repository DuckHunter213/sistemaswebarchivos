<?#Certificados/Index?>
@extends('layouts.app')

@section('content')
<script src="{{ asset('js/certificados/index.js') }}" defer></script>

<div class="container">
  <div class="row justify-content-center">
    <h1 class="text-center">Administracion de certificado personal</h1>
    <div class="container">
      @if (auth()->user()->certificated)
        <div class="row">
          <!--Descomentar para ver la llave privada
            <label class="col">Llave Privada</label>
            <a class="col" href="../storage/{{auth()->user()->email}}/certificados/privateKey.key">Ver llave</a>
          -->
          <label class="col">Llave Privada Encriptada</label>
          <a class="col" href="../storage/{{auth()->user()->email}}/certificados/privateEncryptKey.pem" download>Descargar Llave</a>
          <label class="col">Llave Publica</label>
          <a class="col" href="../storage/{{auth()->user()->email}}/certificados/publicKey.pem" download>Descargar llave</a>
        </div>
      @else
        <p class="box-red" >
          Las llaves son auto generadas por el sistema una vez ingresando la contraseña en el formulario de abajo,
          se dara de alta la firma y pasará a ser un usuario valido. Una vez firmado los certificados 
          no se volverán a generar y la llave privada será destruida del sistema, la cual solo debera conservar usted.
        </p>
        <form name="crearCertificado" action="{{route('certificados.create')}}" method="post">
          @csrf
          <div class="form-group">
            <label>Contraseña</label>
            <input class="form-control" type="password" name="password" id="password">
          </div>
          <div class="form-group">
            <label>Confirmar Contraseña</label>
            <input class="form-control" type="password" name="password-check" id="passwordCheck">
            <small id="passwordHelp" class="text-danger"></small>
          </div>
          <div class="form-check">
            <label class="form-check-label"><input class="form-check-input" type="checkbox" name=""><a href="#">Acepto los terminos y condiciones</a></label>
          </div>
          <div class="form-check">
            <label class="form-check-label"><input class="form-check-input" type="checkbox" name=""><a href="#">He leido el acuerdo de privacidad de datos</a></label>
          </div>
          <input class="btn btn-primary float-right" type="submit" name="" id="crearCertificadoButton">
        </form>
      @endif
    </div>
  </div>
</div>
@endsection
