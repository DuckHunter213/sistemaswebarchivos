@extends('layouts.app')

@section('content')
<script type="text/javascript">
  function asignarValor(valor){
    var documento = document.getElementById('documento');
    var documento_form = document.getElementById('documento_form');
    documento.value = valor;
    documento_form.submit();
  }
</script>
<div class="container">
  <div class="row justify-content-center">
    <h1>Archivos cargados</h1>
    <div class="col-12">  
      @if (empty($documentos))
        <h2>No tienes documentos cargados</h2>
      @else
      <table class="table">
        <thead>
        <tr>
          <th>Documento</th>
          <th>Estatus</th>
          <th>Acciones</th>
        </tr>  
        </thead>
        <tbody>
          <!--TODO:
          <tr>
            <td>Ejemplo1</td>
            <td><span class="badge badge-pill badge-success">Listo</span></td>
            <td><button class="btn btn-primary">Ver Documento</button></td>
          </tr>
          <tr>
            <td>Ejemplo2</td>
            <td><span class="badge badge-pill badge-danger">Rechazado</span></td>
            <td><button class="btn btn-primary">Ver Documento</button></td>
          </tr>
          -->
            @foreach($documentos as $documento)
              <tr>
                <td>{{$documento->nombreDocumento}}</td>
                <td><span class="badge badge-pill badge-light">Pendiente</span></td>
                <td><button onclick="asignarValor('{{$documento->id}}')" class="btn btn-primary">Ver Documento</button></td>
              </tr>
            @endforeach
          @endif
        </tbody>
      </table>
      <form id="documento_form" name="documento_form" method="POST" action="details">
        {{ csrf_field() }}
        <input type="hidden" id="documento" name="documento" value="">
      </form>
    </div>
    <h1>Documentos Solicitados</h1>
    <div class="col-12"> 
    @if(empty($documentosCompartidos))
      <h2>No te han compartido documentos</h2>
    @else 
      <table class="table">
        <thead>
        <tr>
          <th>Documento</th>
          <th>Estatus</th>
          <th>Acciones</th>
        </tr>  
        </thead>
        <tbody>
          @foreach($documentosCompartidos as $documento)
            <tr>
              <td>{{$documento->nombreDocumento}}</td>
              <td><span class="badge badge-pill badge-light">Pendiente</span></td>
              <td><button onclick="asignarValor('{{$documento->id}}')" class="btn btn-primary">Ver Documento</button></td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
      <form id="documento_form" name="documento_form" method="POST" action="details">
        {{ csrf_field() }}
        <input type="hidden" id="documento" name="documento" value="">
      </form>
    </div>
  </div>
</div>

@endsection
