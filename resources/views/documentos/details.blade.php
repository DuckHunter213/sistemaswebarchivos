@extends('layouts.app')
@section('content')
<script type="text/javascript" src="{{ asset('js/documentos/details.js') }}" defer></script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Firmar Documento</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form name="signRequest" id="signRequest" method="POST" action="{{route('documentos.sign')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="validatedCustomFile" name="firmaElectronica" required>
            <label class="custom-file-label" for="validatedCustomFile">Carga tu firma digital (eSignatiSing.pem)</label>
            <small id="validatedCustomFileFeedback" class="text-danger"></small>
          </div>
          <input style="margin-top: 1em;" type="password" name="passwordCertificated" class="form-control" id="passwordCertificated" placeholder="Contraseña">
          <small id="passwordCertificatedFeedback" class="text-danger"></small>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="botonCancelar" data-dismiss="modal">Cancelar</button>
          <button type="button" class="btn btn-primary" id="botonFirmar">Firmar</button>
        </div>
        <input type="hidden" name="idDocumento" value="{{$documento->id}}">
      </form>
    </div>
  </div>
</div>

<div class="container">
  @if (auth()->user()->certificated)
    <div class="row" style="min-height: 33rem;">
      <!--PDF-->
      <embed class="col" id="plugin" type="application/pdf" src="{{$documento->urlDocumento}}#toolbar=0&navpanes=0&scrollbar=0" background-color="0xFF525659" class="col-6" javascript="allow">
      <!--Detalles del documento-->
      <div class="col">
        <div class="container">
          <div class="row align-items-start">
            <h2 class="col-12">Detalles del documento</h2>
            <label class="col-12"><strong> Solicitante del documento: </strong></label>
            <label class="col-12">{{$solicitante->name}} ({{$documento->solicitante}})</label>
            <label class="col-12"><strong> Huella digital del documento: </strong> </label>
            <label class="col-12">{{$documento->huellaDigital}}</label>
            <h3 class="col-12">Firmantes del documento</h3>
          </div>
          <div class="row align-items-center">
            <h4 class="col-12">Firmados</h4>
            <div class="container">
              @foreach($firmas as $firma)
                <label class="col-12">gomez.alejandre@gmail.com</label>
              @endforeach
              <label class="col-12">gomez.alejandre@gmail.com</label>
            </div>
            <h4 class="col-12">Pendientes de firma</h4>
            <div class="col-12">
              <div class="col-12">
                @foreach($firmantes as $firmante)
                  <label  class="col-12">Firmante: {{$firmante}}</label>
                @endforeach
                <label class="col-12">gomez.alejandre@gmail.com</label>
              </div>
            </div>
          </div>
          <div class="row align-items-end">
            <div class="float-right col-12">
              @if(auth()->user()->email == $documento->solicitante)
                <button class="btn btn-info">Solicitar Firmantes</button>
                <button id="botonImprimir" class="btn btn-info">Imprimir documento</button>
              @endif
              <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Firmar</button>
              <input type="hidden" name="documentoId" id="documentoId" value="{{$documento->id}}">
              <input type="hidden" name="token" id="token" value="{{csrf_token()}}">
              <input type="hidden" name="usuarioEmail" id="usuarioEmail" value="{{$documento->solicitante}}">
            </div>
          </div>
        </div>
      </div>
    </div>
  @else
    <h1>Al parecer todavia no validas tu cuenta.</h1>
    <p>Validala ingresando un contraseña para generar tu firma <a href="{{route('certificados.index')}}">acá</a></p>
  @endif  
</div>
@endsection
