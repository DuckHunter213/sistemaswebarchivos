@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Subir Archivos</h1>
    </div>
    <label>Solo se admiten ciertos tipos de formatos</label>
    <form method="POST" action="{{route('documentos.subir')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="file" name="documento">
        <input type="submit" name="subir" value="Subir archivo">
    </form>
</div>
@endsection
