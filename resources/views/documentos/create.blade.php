<?#Documentos/Create?>
@extends('layouts.app')
<link href="{{ asset('css/CustomInputFile/css/normalize.css' ) }}" rel="stylesheet">
<link href="{{ asset('css/CustomInputFile/css/demo.css' ) }}" rel="stylesheet">
<link href="{{ asset('css/CustomInputFile/css/component.css' ) }}" rel="stylesheet">

@section('content')
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<script type="text/javascript">   </script>
<script src="{{ asset('js/documentos/create.js') }}" defer></script>


<div class="container">
  <h1 class="row justify-content-center">Subir Archivos</h1>
  
  @if (true)
  <form method="POST" action="{{route('documentos.subir')}}" enctype="multipart/form-data">
    <div class="row">
    {{ csrf_field() }}
      <div class="col">
        <h2>Paso 1: Subir Archivos</h2>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="validatedCustomFile" name="documento" required>
            <label class="custom-file-label" for="validatedCustomFile">Escoje un documento...</label>
            <div class="invalid-feedback">Solo se admiten los formatos *.pdf</div>
          </div>
      </div>

    <div class="col">
      <h2>Paso 2: Solicitar firmantes</h2>
      <label >Ingresa el Nombre del firmante a solicitar:</label>
      <input class="form-control" id="nombreSolicitado" name="" placeholder="ej. Ana Cárdenas">
      <label >Ingresa el Email del firmante a solicitar:</label>
      <input class="form-control" id="correo" type="email" name="" placeholder="ej. correo@servicio.com">
      <button type="button" id= "agregar" class="btn btn-secondary float-right">Agregar Firmante</button>
      </div>
    </div>

    <h2>Paso 3: Corroborar la información</h2>
      <table class="table" id="tablaFirmantes">
        <thead>
          <tr>
            <th>Nombre del Firmante</th>
            <th>Correo</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
      <input class="btn btn-primary float-right" type="submit" name="subir" value="Subir archivo">
      <input type='hidden' id='filas' name='numeroFilas' value=0>
      <div id='filasRegistradas' hidden></div>
    </form>
  @else
  @endif
</div>
@endsection




