<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Signati Litteris') }}</title>
    <!-- Scripts -->
      <!--FileSaver.js-->
      <script  src="{{ asset('js/FileSaver.js') }}" defer></script>
      <!--App.js-->
      <script src="{{ asset('js/app.js') }}" defer></script>
      <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
      <script src="{{ asset('js/chat/global.js') }}" ></script>
      <script src="{{ asset('js/complement.js') }}" defer></script>
      <!--Validate.js-->
      <script src="{{ asset('js/validate_es.js') }}" defer></script>
      <!--<script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.12.0/validate.min.js"></script>-->
      
      <!--Jquery-->
      <script  src="https://code.jquery.com/jquery-3.3.1.min.js"  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="  crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!--Awasome Font-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/complement.css' ) }}" rel="stylesheet">
    <!--<link href="{{ asset('css/custom-theme-black.css') }}" rel="stylesheet">-->
  </head>
  <body>
    <div id="app">
      <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
          <a class="navbar-brand" href="{{ url('/') }}">{{config('app.name', 'Signati Litteris')}}</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            @guest
            @else
            <ul class="navbar-nav mr-auto">
            <!--Menu Documentos-->
            @role('coordinador|administrador')
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>Documentos<span class="caret"></span></a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="nav-link" href="{{ route('documentos.index') }}">Estatus Documentos</a>
                <a class="nav-link" href="{{ route('documentos.create') }}">Nuevo Documento</a>
              </div>
            </li>
            <!--Menu Usuarios-->
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>Usuarios<span class="caret"></span></a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="nav-link" href="{{route('user.index')}}">Administrar</a>
              </div>
            </li>
            @endrole
            </ul>
            @endguest
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
              <!-- Authentication Links -->
              @guest
              <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">Iniciar Sesión</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">Registro</a>
              </li>
              @else
              <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><i class="fas fa-bell"></i>  <span class="badge badge-pill badge-danger">1</span></a>
                <div class="dropdown-menu dropdown-menu-right notificacion" aria-labelledby="navbarDropdown">
                  <a class="nav-link" href="#">
                    <div class="card"> 
                      <div class="card-header red">Peticion de firma <i class="fas fa-file-signature"></i></div>
                      <div class="card-body">El usuario <strong>gomez.alejandre@gmail.com</strong> ha solicitado por <strong>2 vez</strong> tu firma en el documento <strong>nombreGenialDelDocumento.pdf</strong></div>
                    </div>
                  </a>
                  <a class="nav-link" href="#">
                    <div class="card"> 
                      <div class="card-header red">Peticion de firma <i class="fas fa-file-signature"></i></div>
                      <div class="card-body">El usuario <strong>gomez.alejandre@gmail.com</strong> ha solicitado tu firma en el documento <strong>nombreGenialDelDocumento.pdf</strong></div>
                    </div>
                  </a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{route('certificados.index')}}">Administrar Perfil</a>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Cerrar Sesión</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                </div>
              </li>
              @endguest
            </ul>
          </div>
        </div>
      </nav>
      <main class="py-4">
        @yield('content')
        @guest
        @else
        <!--TODO:Cambiar a componente-->
        <div id="componenteChat">
          <div class="btn-flotante" id="btnChatFlotante">
            <button class="btn btn-warning" onclick="$('#chatFlotante').show();$('#btnChatFlotante').hide();">Chat</button>
          </div>

          <div class="flotante" id="chatFlotante" style="display: none;">
            <div id="barraNavegacion">
              <div class="container">
                <div class="row">
                  <div class="col"></div>
                  <div class="col"><button style="margin-top: .5em;" class="float-right btn btn-danger" onclick="$('#chatFlotante').hide();$('#btnChatFlotante').show();">X</button></div>
                </div>
              </div>
            </div>  
            <!--parte central-->
            <div id="chat" >
              <div id="mensajes">
              </div>
            </div>
            <!--Parte final-->
            <div class="container">
              <div id="areaMensaje">
                <div class="form-row align-items-center">
                  <input type="text" class="row form-control" name="inputMensaje" id="inputMensaje" autocomplete="off">
                  <div class="row btn btn-primary" onclick="mandarMensajePublico('{{route('chat.mandarMensaje')}}','{{csrf_token()}}');"><i class="fas fa-angle-double-right"></i></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endguest
      </main>
    </div>
  </body>
</html>