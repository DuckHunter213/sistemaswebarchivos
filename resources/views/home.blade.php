@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">Dashboard</div>
        <div class="card-body">
          @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif
          Si ves esta página es por que te registraste correctamente...
          Mientras ocupa nuestro chat
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
