@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              <div class="card-header">Verifica tu correo electronico</div>
              <div class="card-body">
                @if (session('resent'))
                  <div class="alert alert-success" role="alert">Se reenvio otro email de confirmación a tu correo</div>
                @endif
                Antes de proceder, por favor checa tu email de verificación
                si no recibiste el mensaje <a href="{{ route('verification.resend') }}">haz click aquí para reenviar el mensaje</a>.
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
