<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <h3><strong>Solicitud de firma </strong></h3>

    <p align="justify">
        Estimado(a) {{ $name }} :<br>
        Se ha solicitado que usted firme el archivo {{ $archivo }}<br>
        en el sistema Signati Litteris al que puede acceder por<br>
        medio de <a href="http://127.0.0.1:8000/login">este enlace</a>, en caso de no tener una cuenta, <br>
        puede registrase gratuitamente.
    </p>

</body>
</html>