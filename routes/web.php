<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
*/

Route::get('/', function () {
  return view('welcome');
});

Route::get('home',function(){
  return redirect(route('home.index'));
});
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

//Home
Route::get('/home/index', 'HomeController@index')->name('home.index');
Route::get('/home/test', 'HomeController@test')->name('home.test');

//Chat
Route::get('/chat/index', 'ChatController@index')->name('chat.index');
//Route::get('/chat/mandarMensaje', 'ChatController@mandarMensaje')->name('mandarMensaje');
//Route::post('/chat/mandarMensaje/{message}', 'ChatController@mandarMensaje')->name('chat.mandarMensaje');
Route::post('/chat/mandarMensaje', 'ChatController@mandarMensaje')->name('chat.mandarMensaje');

//Documentos
Route::get('/documentos/index', 'DocumentosController@index')->name('documentos.index');

Route::get('/documentos/create', 'DocumentosController@create')
  ->name('documentos.create')
  ->middleware('role:coordinador');
  
Route::post('/documentos/generarPDF', 'DocumentosController@generarPDF')->name('documentos.generarPDF');
Route::post('/documentos/PDFFirmado', 'DocumentosController@PDFFirmado')->name('documentos.PDFFirmado');
Route::get('/documentos/test', 'DocumentosController@test')->name('documentos.test');
Route::post('/documentos/details', 'DocumentosController@details')->name('documentos.details');
Route::post('/documentos/subirDocumento', 'DocumentosController@subirDocumento')->name('documentos.subir');
Route::post('/documentos/firmarDocumento', 'DocumentosController@firmarDocumento')->name('documentos.sign');

//Certificados
Route::get('/certificados/index','CertificadosController@index')->name('certificados.index');
Route::post('/certificados/create','CertificadosController@create')->name('certificados.create');

//Usuarios 
Route::get('/user/index','UserController@index')->name('user.index');
Route::get('/user/giveAdminControl','UserController@giveAdminControl')->name('user.giveAdminControl');


Auth::routes(['verify' => true]);