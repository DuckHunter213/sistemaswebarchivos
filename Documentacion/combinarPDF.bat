@echo off

cd %1
gswin64c -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=%2 "%3" "%4"