// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;

var pusher = new Pusher('b7143dca9285dd0e25e9', {
  cluster: 'us2',
  forceTLS: true
});

var presenceChannel = pusher.subscribe('presence-signatiLitteris');
presenceChannel.bind('pusher:subscription_succeeded', function() {
  var me = presenceChannel.members.me;
  var userId = me.id;
  var userInfo = me.info;
});


$('input[type="file"]').change(function(e){ var fileName = e.target.files[0].name; $('.custom-file-label').html(fileName); $("#validatedCustomFile").removeClass("is-invalid"); $("#validatedCustomFileFeedback").text(""); });