var filas=0;
  $(document).ready(function(){
    $("#agregar").click(function(){
      if(nombreVacio()){
        alert("Por favor ingrese un nombre");
      }else if(correoVacio()){
        alert("Por favor ingrese un correo");
      }else{
        var nombre = $("#nombreSolicitado").val();
        var correo = $("#correo").val();
        var fila = "<tr ='"+filas+"'><td>" + nombre + "</td><td>" + correo + "<td><button class='btn btn-danger' id='remover' onClick='event.preventDefault();'>Remover Firmante</button></td></tr>";
        var filaInput = "<input type='hidden' name='firmante"+filas+"' value='"+correo+"'>";
        filas++;
        $("table tbody").append(fila);
        $("#filasRegistradas").append(filaInput);
        $('#filas').val(filas);
      }
    });

    $("table tbody").on('click', '#remover', function(){
      var actual=$(this).closest('tr').index();//1
      $("input[name='firmante"+(actual)+"']").remove();//actual+1= t-2, firmante2
      $(this).closest('tr').remove();//t-1
      if(actual+1<filas){ //2<3
        var i;
        for(i = actual+1; i < filas; i++){
          $("input[name='firmante"+(i)+"']").attr('name', 'firmante'+(i-1));
          //$("input[name='firmante"+(actual+2)+"']").attr('name', 'firmante'+(actual+1));
        }
      }
      filas--;
      $('#filas').val(filas);
    });
  }); 

  function nombreVacio(){
    valor = document.getElementById("nombreSolicitado").value;
    if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
      return true;
    }else{
        return false;
    }
  } 
  function correoVacio(){
    valor = document.getElementById("correo").value;
    if( valor == null || valor.length == 0 || /^\s+$/.test(valor) ) {
      return true;
    }else{
        return false;
    }
  } 