$(document).ready(function(){
  
  function openWindowWithPost(url, data) {
    var form = document.createElement("form");
    form.target = "_blank";
    form.method = "POST";
    form.action = url;
    form.style.display = "none";

    for (var key in data) {
      var input = document.createElement("input");
      input.type = "hidden";
      input.name = key;
      input.value = data[key];
      form.appendChild(input);
    }

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
  }

  $("#botonCancelar").click(function() {
    event.preventDefault();
  });
  $("#botonFirmar").click(function() {
    var valido = true;
    if($('#validatedCustomFile').get(0).files.length === 0){
      $("#validatedCustomFile").addClass("is-invalid");
      $("#validatedCustomFileFeedback").text("Se necesita agregar un archivo");
      valido = false;
    }
    if($("#passwordCertificated").val() == ""){  
      $("#passwordCertificated").addClass("is-invalid");
      $("#passwordCertificatedFeedback").text("Se necesita la contraseña del certificado");
      valido = false;
    }else{
      $("#passwordCertificated").removeClass("is-invalid");
      $("#passwordCertificatedFeedback").text("");
    }if(valido){
      $("#signRequest").submit();
    }
  });
  $("#botonImprimir").click(function(){
    $(document).css({'cursor' : 'wait'});
    console.log($("#usuarioEmail").val());
    $("#botonImprimir").attr("disabled",true);
    $.post( "/documentos/generarPDF", { idDocumento: $('#documentoId').val(), _token: $('#token').val()})
    .done(function( data ) {
      openWindowWithPost("/documentos/PDFFirmado",{
        idDocumento: $('#documentoId').val(),
        _token: $('#token').val(),
        usuarioEmail: $('#usuarioEmail').val(),
      });
    }).fail(function() {
      console.log( "Error al generar el PDF Firmado" );
    }).always(function(){
      $(document.body).css({'cursor' : 'default'});
      $("#botonImprimir").attr("disabled",false);
    });
  });  
});