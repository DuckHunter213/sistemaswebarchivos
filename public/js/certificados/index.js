$(document).ready(function() {
  $("#crearCertificadoButton").click(function(event)
  {
    var password = $("#password");
    var passwordCheck = $("#passwordCheck");
    //TODO: utilizar validate js o separarlo en un método
    //Validaciones del las contraseñas que sean correctas.
    if(password.val() == "" || passwordCheck.val() == ""){
      $("#password").addClass("is-invalid");
      $("#passwordCheck").addClass("is-invalid");
      $("#passwordHelp").text("Las contraseñas no pueden ser vacias.");
      event.preventDefault();
    }else{
      if(password.val() == passwordCheck.val()){
        $("#password").removeClass("is-invalid");
        $("#passwordCheck").removeClass("is-invalid");
        $("#passwordHelp").text("");
        $("#crearCertificado").submit();
      }else{
        $("#password").addClass("is-invalid");
        $("#passwordCheck").addClass("is-invalid");
        $("#passwordHelp").text("Las contraseñas no coinciden.");
        event.preventDefault(); 
      }
    }
  });
});