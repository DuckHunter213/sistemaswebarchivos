"use strict";
// Enable pusher logging - don't include this in production
//$(document).ready(function() {
Pusher.logToConsole = true;

var pusher = new Pusher('b7143dca9285dd0e25e9', {
  cluster: 'us2',
  forceTLS: true
});

var channel = pusher.subscribe('my-channel');
channel.bind('App\\Events\\HolaPusherEvent', function(data) {
  //alert(JSON.stringify(data));
  var lista = $('#mensajes');
  //creando elementos
  var mensajePropio = document.createElement("div");
  var usuario = document.createElement("small");
  var salto = document.createElement("br");
  var mensaje = document.createElement("p");
  //Agregando textos
  $(usuario).text("gomez.alejandre@gmail.com");
  $(mensaje).text(data.message);
  //
  $(usuario).appendTo(mensajePropio);
  $(salto).appendTo(mensajePropio);
  $(mensaje).appendTo(mensajePropio);
  $(mensaje).addClass("mensaje-margin-zero");
  $(mensajePropio).addClass("mensaje mensajePropio");
  $(mensajePropio).appendTo(lista);
});

  function mandarMensajePublico(url,token){
    var mensaje = $("#inputMensaje");
    //alert(mensaje);
    $.ajax({
      type: "POST",
      url: url,
      data: {  message: mensaje.val(), _token: token },
      success: function (data) {
        console.log(data);
      },
      error: function (data, textStatus, errorThrown) {
        console.log(data);
      },
    });
    mensaje.val("");
  }
//});